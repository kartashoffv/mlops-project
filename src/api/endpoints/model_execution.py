import os
import pickle
import pandas as pd
from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import FileResponse
from sqlalchemy.orm import Session
from src.db import models, database
from pydantic import BaseModel
from .auth import get_current_user
from .billing import deduct_balance


router = APIRouter()

# Directory where uploaded files are stored
UPLOAD_DIR = '/var/project/mlops-project/data/user/uploaded_files/'
# os.makedirs(UPLOAD_DIR, exist_ok=True)

MODEL_DIR = '/var/project/mlops-project/models/'


class RunModelRequest(BaseModel):
    data_id: str
    model_name: str
    model_path: str
    model_cost: int


def run_model(data, file_path):
    with open(file_path, 'rb') as file:
        model = pickle.load(file)
    predictions = model.predict(data)

    return predictions


@router.post("/run-model")
async def execute_model(
        request: RunModelRequest,
        db: Session = Depends(database.get_db),
        current_user: models.User = Depends(get_current_user)):
    data_file_path = os.path.join(UPLOAD_DIR, f"{request.data_id}.csv")
    model_file_path = os.path.join(MODEL_DIR, request.model_path)

    if not os.path.exists(data_file_path):
        raise HTTPException(status_code=404, detail="Uploaded data not found")

    data = pd.read_csv(data_file_path)
    # try:
    predictions = run_model(data, f"{model_file_path}")
    deduct_balance(current_user.id, request.model_cost, db)
    output = pd.DataFrame(predictions, columns=['Status'])
    # download_path = f"prediction_{request.data_id}.csv"
    download_path = ("/var/project/mlops-project/data/user/downloaded_files"
                     f"/prediction_{request.data_id}.csv")
    output.to_csv(download_path)

    return {
        "model_name": request.model_name,
        "price": request.model_cost,
        "predictions": f"{predictions}",
        "file_path": download_path,
        "data": f"{output}"
    }


@ router.get("/{file_path:path}")
async def download(file_path: str):
    # Debug: Print the file_path received
    print(f"Received file path: {file_path}")

    # Normalize and get the absolute file path
    absolute_file_path = os.path.abspath(file_path)
    print(f"Absolute file path: {absolute_file_path}")

    # Define the allowed base path
    allowed_base_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), '..', '..'))
    print(f"Allowed base path: {allowed_base_path}")

    # Check if the file path is within the allowed base directory
    if not absolute_file_path.startswith(allowed_base_path):
        print("File path is outside the allowed base directory.")
        raise HTTPException(status_code=403, detail="Forbidden")

    # Check if the file exists at the constructed path
    if os.path.exists(absolute_file_path):
        print("File exists. Proceeding to download.")
        return FileResponse(
            path=absolute_file_path,
            filename=os.path.basename(absolute_file_path),
            media_type='application/octet-stream')
    else:
        print("File not found.")
        raise HTTPException(status_code=404, detail="File not found")
