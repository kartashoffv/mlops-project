import click
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler, OneHotEncoder
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from catboost import CatBoostClassifier
import pickle


class ModelTrainer:
    def __init__(self, data_path):
        self.data_path = data_path
        self.preprocessor = None
        self.models = {
            "RandomForest": RandomForestClassifier(),
            "LogisticRegression": LogisticRegression(),
            "CatBoost": CatBoostClassifier(
                iterations=515,
                depth=10,
                learning_rate=0.24180265351139493,
                l2_leaf_reg=6.857281392013732,
                verbose=False
            )
        }

    def load_data(self):
        data = pd.read_csv(self.data_path)
        X = data.drop(['Status', 'ID'], axis=1)
        y = data['Status']
        le = LabelEncoder()
        y = le.fit_transform(y)
        return train_test_split(X, y, test_size=0.2, random_state=42)

    def preprocess_data(self, X):
        numeric_features = X.select_dtypes(
            include=['int64', 'float64']).columns
        categorical_features = X.select_dtypes(include=['object']).columns

        numeric_transformer = Pipeline(steps=[
            ('imputer', SimpleImputer(strategy='mean')),
            ('scaler', StandardScaler())
        ])

        categorical_transformer = Pipeline(steps=[
            ('imputer', SimpleImputer(strategy='most_frequent')),
            ('onehot', OneHotEncoder(handle_unknown='ignore'))
        ])

        self.preprocessor = ColumnTransformer(
            transformers=[
                ('num', numeric_transformer, numeric_features),
                ('cat', categorical_transformer, categorical_features)
            ]
        )

    def train_and_save_models(self, X_train, y_train):
        for model_name, model in self.models.items():
            pipeline = Pipeline(steps=[
                ('preprocessor', self.preprocessor),
                ('classifier', model)
            ])
            pipeline.fit(X_train, y_train)
            with open(('/var/project/mlops-project/'
                       f'models/{model_name}.pkl', 'wb')) as file:
                pickle.dump(pipeline, file)

    def run(self):
        X_train, X_test, y_train, y_test = self.load_data()
        self.preprocess_data(X_train)
        self.train_and_save_models(X_train, y_train)


@click.command()
@click.option('--data-path',
              default=('/var/project/mlops-project/'
                       'data/processed/cirrhosis.csv'),
              help='Path to the data file')
def main(data_path):
    trainer = ModelTrainer(data_path=data_path)
    trainer.run()


if __name__ == "__main__":
    main()
