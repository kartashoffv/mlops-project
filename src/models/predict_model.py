import pandas as pd
import pickle
import click


class ModelPredictor:
    def __init__(self, model_path):
        self.model_path = model_path
        self.expected_columns = ['N_Days', 'Drug', 'Age', 'Sex',
                                 'Ascites', 'Hepatomegaly', 'Spiders',
                                 'Edema', 'Bilirubin', 'Cholesterol',
                                 'Albumin', 'Copper', 'Alk_Phos', 'SGOT',
                                 'Tryglicerides', 'Platelets', 'Prothrombin',
                                 'Stage']
        self.model = self.load_model()

    def load_model(self):
        with open(self.model_path, 'rb') as file:
            return pickle.load(file)

    def check_columns(self, data):
        if not all(column in data.columns for column in self.expected_columns):
            missing_columns = set(self.expected_columns) - set(data.columns)
            raise ValueError(f"Missing columns: {missing_columns}")

    def make_prediction(self, data):
        self.check_columns(data)
        return self.model.predict(data)


def make_predictions(data_path, model_paths):
    data = pd.read_csv(data_path)
    results = {}
    for model_path in model_paths:
        predictor = ModelPredictor(model_path)
        predictions = predictor.make_prediction(data)
        results[model_path] = predictions
    return results


@click.command()
@click.option('--data-path',
              default=('/var/project/mlops-project/data'
                       '/raw/cirrhosis_test.csv'),
              help='Path to the test data file')
@click.option('--model-paths', multiple=True, help='Paths to the model files')
def main(data_path, model_paths):
    results = make_predictions(data_path, model_paths)
    for model_path, predictions in results.items():
        print(f"Model: {model_path}\nPredictions: {predictions}\n")


if __name__ == "__main__":
    main()
